#ifndef SNAKE_H
#define SNAKE_H

#include "utils.h"


typedef struct{
    //int direction;
    int x, y;
}snake_part;

typedef struct{
    bool died;
    int ID;
    int head;
    int tail;
    int size;
    int capacity;
    snake_part *snake;
    int direction; //0 right, 1 down, 2 left, 3 up
} snake_front;

snake_front* makeSnake(int player);

bool checkCollisionBorder(snake_front*player);

bool checkCollisionPlayer(int x_pos, int y_pos, game_space* game_board);

void growSnake(snake_front* player);

#endif