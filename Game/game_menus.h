#ifndef GAME_MENUS_H
#define GAME_MEUS_H

#include "game_loop.h"
#include <stdint.h>
#include <stdlib.h>
#include "mzapo_regs.h"
#include "utils.h"
#include "mzapo_parlcd.h"
#include <time.h>
#include "font_types.h"
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>


int game_over_screen(game_struct* game, int score);
int pause_menu(game_struct* game, int score);

#endif