/*contains the main game loop and game board related methods*/

#include "game_loop.h"
#include <stdint.h>
#include <stdlib.h>
#include "mzapo_regs.h"
#include "utils.h"
#include "mzapo_parlcd.h"
#include <time.h>
#include "font_types.h"
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include "game_menus.h"
#include "snake.h"
#include "LED_switch.h"

#define SIZE 20

game_space* prepBoard();
void nextFrame(game_struct* game, game_space* game_board, struct timespec loop_delay);
bool boardUpdate(snake_front*player, game_space* game_board);

//main game loop, regularly moves player/s, checks for collisions and updates the LCD display
int gameLoop(game_struct* game, bool versus){
    sleep(1);
    struct timespec loop_delay;
    loop_delay.tv_sec = 0;
    loop_delay.tv_nsec = game->speed * 1000 * 1000;
    snake_front *player = makeSnake(1);
    snake_front *player2 = NULL;
    if(!player){
        fprintf(stderr, "error, failed to ceate player snake!\n");
        exit(-1);
    }
    //2 player option chosen, spawning 2nd snake
    if(versus){
        player2 = makeSnake(2);
        if(!player2){
            fprintf(stderr, "error, failed to ceate player snake!\n");
            exit(-1);
        }
    }
    game_space* game_board = prepBoard();
    if(!game_board){
        fprintf(stderr, "error, failed to create game board!\n");
        exit(-1);
    } 
    int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
    int R_dir = ((r>>16)&0xff);
    int B_dir = (r&0xff);
    game_board->fruit_present = false;
    LEDreset();
    bool p1_dead = false;
    bool p2_dead = false;
    while (1) {
        //check if fruit is spawned
        if(!game_board->fruit_present){
            if(!spawn_fruit(game_board)){
                fprintf(stderr, "error, fruit cannot be spawned!\n");

            }else{
                game_board->fruit_present = true;
            }
        }
        r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if(R_dir-((r>>16)&0xff) <= -4){

            //change direction right
            R_dir = ((r>>16)&0xff);
            player->direction++;
            if(player->direction > 3){
                player->direction = 0;
            }
        }else if(R_dir-((r>>16)&0xff) >= 4){

            //change direction left
            R_dir = ((r>>16)&0xff);
            player->direction--;
            if(player->direction < 0){
                player->direction = 3;
            }
        }
        if(player2){
            if(B_dir-(r&0xff) <= -4){
                //change direction right
                B_dir = (r&0xff);
                player2->direction++;
                if(player2->direction > 3){
                    player2->direction = 0;
                }
            }else if(B_dir-(r&0xff) >= 4){
                //change direction left
                B_dir = (r&0xff);
                player2->direction--;
                if(player2->direction < 0){
                    player2->direction = 3;
                }
            }
        }
        r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if ((r&0x7000000)!=0) {
            sleep(1);
            int ret = pause_menu(game, player->size);
            if(ret == 1){
                return ret;
            }
        }
        //check for collisions
        p1_dead = !boardUpdate(player, game_board);
        p2_dead = !boardUpdate(player2, game_board);
        if(p1_dead || p2_dead){
            int score = 0;
            if(p1_dead && p2_dead){
                if(player->size > player2->size){
                    setLED(0, 255, 0, true);
                    setLED(255, 0, 0, false);
                    score = player->size;
                }else if(player->size < player2->size){
                    setLED(0, 255, 0, false);
                    setLED(255, 0, 0, true);
                    score = player2->size;
                }else{
                    setLED(255, 0, 0, true);
                    setLED(255, 0, 0, false);
                    score = player->size;
                }
            }else if(p1_dead && !player2){
                    setLED(255, 0, 0, true);
                    score = player->size;
            }else if(p1_dead && player2){
                    setLED(0, 255, 0, false);
                    setLED(255, 0, 0, true);
                    score = player2->size;
            }else{
                    setLED(0, 255, 0, true);
                    setLED(255, 0, 0, false);
                    score = player->size;
            }
            sleep(1);
            int ret = game_over_screen(game, score); //can recieve 1 or 2
            return ret;
        }
        if(!versus){
            diodsSet(player->size);
        }
        nextFrame(game, game_board, loop_delay);
    }
}

//initialize the game board structure
game_space* prepBoard(){
    game_space* ret = malloc(sizeof(game_space));
    if(!ret){
        fprintf(stderr, "malloc error\n");
        return ret;
    }
    ret->width = 480/SIZE;
    ret->height = 320/SIZE;
    ret->board = calloc(ret->width*ret->height, sizeof(int));
    return ret;
}

//generate next frame to be displayed
void nextFrame(game_struct* game, game_space* game_board, struct timespec loop_delay){
    bool fruit_rendered = false;
    for (int i = 0; i < game_board->height; i++) {
        for(int j = 0; j < game_board->width; j++){
            switch(*(game_board->board+(i*game_board->width+j))){
                case 0: //no snake
                    for(int k = 0; k < SIZE; k++){
                        for(int l = 0; l < SIZE; l++){
                            draw_pixel(j*SIZE+l, i*SIZE+k, 0xffff, game->fb);
                        }
                    }
                    break;
                case 1: //snake present
                    for(int k = 0; k < SIZE; k++){
                        for(int l = 0; l < SIZE; l++){
                            draw_pixel(j*SIZE+l, i*SIZE+k, 0xf800, game->fb);
                        }
                    }
                    break;
                case 2:
                    for(int k = 0; k < SIZE; k++){
                        for(int l = 0; l < SIZE; l++){
                            draw_pixel(j*SIZE+l, i*SIZE+k, 0x00ff, game->fb);
                        }
                    }
                    break;
                case 3:
                fruit_rendered = true;
                    for(int k = 0; k < SIZE; k++){
                        for(int l = 0; l < SIZE; l++){
                            draw_pixel(j*SIZE+l, i*SIZE+k, 0xff00, game->fb);
                        }
                    }
                default:
            }
        }
    }  
    if(!fruit_rendered){
        game_board->fruit_present = false;
    }
    parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
        for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
            parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
        }

    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    return;
}

//adjusts bard visualizaton based on the plaer movement
bool boardUpdate(snake_front*player, game_space* game_board){
    if(!player){
        return true;
    }
    int x_temp, y_temp;
    int source;
    player->head++;
    if(player->head >= player->capacity){
        player->head = 0;
        source = player->capacity-1;
    }else{
        source = player->head-1;
    }
    switch(player->direction){
        case 0: //right
            x_temp = (player->snake+source)->x+1;
            y_temp = (player->snake+source)->y;
            break;
        case 1: //down
            x_temp = (player->snake+source)->x;
            y_temp = (player->snake+source)->y+1;
            break;
        case 2: //left
            x_temp = (player->snake+source)->x-1;
            y_temp = (player->snake+source)->y;
            break;
        default: //up
            x_temp = (player->snake+source)->x;
            y_temp = (player->snake+source)->y-1;
    }
    if(*(game_board->board+(y_temp*game_board->width+x_temp)) == 3){
        //fruit has been consumed
        *(game_board->board+(y_temp*game_board->width+x_temp)) = player->ID;
        (player->snake+(player->head))->x = x_temp;
        (player->snake+(player->head))->y = y_temp;
        game_board->fruit_present = false;
        setLED(0, 255, 0, player->ID == 1 ? true : false);
        growSnake(player);
    }else{
        if(game_board->fruit_present){
            setLED(0, 0, 0, player->ID == 1 ? true : false);
        }
        *(game_board->board+((player->snake+player->tail)->y*game_board->width+(player->snake+player->tail)->x)) = 0;
        if(checkCollisionPlayer(x_temp, y_temp, game_board)){
            player->died = true;
            return false;
        }
        *(game_board->board+(y_temp*game_board->width+x_temp)) = player->ID;
        (player->snake+(player->head))->x = x_temp;
        (player->snake+(player->head))->y = y_temp;
        player->tail++;
        if(player->tail >= player->capacity){
            player->tail = 0;
        }
        if(checkCollisionBorder(player)){
            return false;
        }
    }
    return true;
}




