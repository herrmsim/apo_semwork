#ifndef MAIN_MENUS_H
#define MAIN_MENUS_H

#include "utils.h"

void mainMenu(game_struct*game);
void speedMenu(game_struct* game);
void playerMenu(game_struct* game);

#endif