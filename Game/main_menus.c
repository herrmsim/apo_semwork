/*methods for menus prior to the gameplay, each method corresponds to one type of menu*/


#include <stdint.h>
#include "game_loop.h"
#include <stdint.h>
#include <stdlib.h>
#include "mzapo_regs.h"
#include "utils.h"
#include "mzapo_parlcd.h"
#include <time.h>
#include "font_types.h"
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include "LED_switch.h"
#include "main_menus.h"


enum speed{SLOW = 300, NORMAL = 200, FAST = 100};

//main menu handler function, option to start the game or quit the program
void mainMenu(game_struct*game){
    struct timespec loop_delay;
    loop_delay.tv_sec = 0;
    loop_delay.tv_nsec = 150 * 1000 * 1000;
    char start[6] = "START";
    char quit[5] = "QUIT";
    int choice = 0;
    bool outline;
    int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
    int G_stat = ((r>>8)&0xff);
    LEDreset();
    while(1){
        r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if ((r&0x7000000)!=0) {
            if(choice == 0){
                sleep(1);
                speedMenu(game);
                sleep(1);
                choice = 1;
            }else{
                LEDreset();
                return;
            }
        }    
        if(G_stat-((r>>8)&0xff) >= 4 || G_stat-((r>>8)&0xff) <= -4){
            G_stat = ((r>>8)&0xff);
            choice = (choice == 0) ? 1 : 0;
        }
        for (game->ptr = 0; game->ptr < 320*480 ; game->ptr++) {
            game->fb[game->ptr]=0xffff;
        }
        outline = choice == 0 ? true : false;
        drawButton(outline, 150, 100, start, 5, game);
        drawButton(!outline, 150, 170, quit, 4, game);
        parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
        for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
        parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
        }

        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL); 
    }
    LEDreset();
    return;
}

//speed selection menu, 3 speed options + BACK button -- returns player to main menu
void speedMenu(game_struct* game){
    struct timespec loop_delay;
    loop_delay.tv_sec = 0;
    loop_delay.tv_nsec = 150 * 1000 * 1000;
    char slow[5] = "SLOW";
    char normal[7] = "NORMAL";
    char fast[5] = "FAST";
    char back[5] = "BACK";
    int choice = 1;
    bool outline;
    int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
    int G_stat = ((r>>8)&0xff);
    LEDreset();
    while(1){
        r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if ((r&0x7000000)!=0){
            switch(choice){
                case 0:
                    game->speed = SLOW;
                    sleep(1);
                    playerMenu(game);
                    return;
                    break;
                case 1:
                    game->speed = NORMAL;
                    sleep(1);
                    playerMenu(game);
                    return;
                    break;
                case 2:
                    game->speed = FAST;
                    sleep(1);
                    playerMenu(game);
                    return;
                    break;
                default:
                    return;
            }
        }
        if(G_stat-((r>>8)&0xff) >= 4){
            G_stat = ((r>>8)&0xff);
            choice--;
            if(choice < 0){
                choice = 3;
            }
        }else if(G_stat-((r>>8)&0xff) <= -4){
            G_stat = ((r>>8)&0xff);
            choice = (choice+1)%4;
        }
        for (game->ptr = 0; game->ptr < 320*480 ; game->ptr++) {
            game->fb[game->ptr]=0xffff;
        }
        outline = choice == 0 ? true : false;
        drawButton(outline, 150, 40, slow, 4, game);
        outline = choice == 1 ? true : false;
        drawButton(outline, 100, 110, normal, 6, game);
        outline = choice == 2 ? true : false;
        drawButton(outline, 150, 180, fast, 4, game);
        outline = choice == 3 ? true : false;
        drawButton(outline, 150, 250, back, 4, game);
        parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
        for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
            parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
        }
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    LEDreset();
    return;
}

//menu for player number selection (1 or 2 players) and a BACK button -- returns player main menu
void playerMenu(game_struct* game){
    struct timespec loop_delay;
    loop_delay.tv_sec = 0;
    loop_delay.tv_nsec = 150 * 1000 * 1000;
    char opt1[9] = "1 PLAYER";
    char opt2[10] = "2 PLAYERS";
    char back[5] = "BACK";
    int choice = 0;
    bool outline;
    int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
    int G_stat = ((r>>8)&0xff);
    LEDreset();
    int play;
    while(1){
        r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if ((r&0x7000000)!=0){
            switch(choice){
                case 0:
                    do{
                        diodsReset();
                        play = gameLoop(game, false);
                        diodsReset();
                    }while(play == 2);
                    return;
                    break;
                case 1:
                    do{
                        play = gameLoop(game, true);
                    }while(play == 2);
                    return;
                    break;
                default:
                    return;
            }
        }
        if(G_stat-((r>>8)&0xff) >= 4){
            G_stat = ((r>>8)&0xff);
            choice--;
            if(choice < 0){
                choice = 2;
            }
        }else if(G_stat-((r>>8)&0xff) <= -4){
            G_stat = ((r>>8)&0xff);
            choice = (choice+1)%3;
        }
        for (game->ptr = 0; game->ptr < 320*480 ; game->ptr++) {
            game->fb[game->ptr]=0xffff;
        }
        outline = choice == 0 ? true : false;
        drawButton(outline, 80, 40, opt1, 8, game);
        outline = choice == 1 ? true : false;
        drawButton(outline, 60, 110, opt2, 9, game);
        outline = choice == 2 ? true : false;
        drawButton(outline, 150, 180, back, 4, game);
        parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
        for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
            parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
        }
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    LEDreset();
    return;
}
