/*auxiliary functions for pixel drawing, button rendering and fruit spawning*/ 

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "utils.h"
#include "font_types.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>


//changes colour of the pixel
void draw_pixel(int x, int y, unsigned short color, unsigned short *fb) {
  if (x>=0 && x<480 && y>=0 && y<320) {
    fb[x+480*y] = color;
  }
}


void draw_pixel_big(int x, int y, unsigned short color, unsigned short *fb, font_descriptor_t *fdes, int scale) {
  int i,j;
  for (i=0; i<scale; i++) {
    for (j=0; j<scale; j++) {
      draw_pixel(x+i, y+j, color, fb);
    }
  }
}

int char_width(int ch, unsigned short *fb, font_descriptor_t *fdes, int scale) {
  int width;
  if (!fdes->width) {
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch-fdes->firstchar];
  }
  return width;
}

//draws a requested char in the desired place in the display
void draw_char(int x, int y, char ch, unsigned short color, unsigned short *fb, font_descriptor_t *fdes, int scale) {
  int w = char_width(ch, fb, fdes, scale);
  const font_bits_t *ptr;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    if (fdes->offset) {
      ptr = &fdes->bits[fdes->offset[ch-fdes->firstchar]];
    } else {
      int bw = (fdes->maxwidth+15)/16;
      ptr = &fdes->bits[(ch-fdes->firstchar)*bw*fdes->height];
    }
    int i, j;
    for (i=0; i<fdes->height; i++) {
      font_bits_t val = *ptr;
      for (j=0; j<w; j++) {
        if ((val&0x8000)!=0) {
          draw_pixel_big(x+scale*j, y+scale*i, color, fb, fdes, scale);
        }
        val<<=1;
      }
      ptr++;
    }
  }
}

//spawn fruit in a free space on the game board
bool spawn_fruit(game_space* game_board) {
    bool ret = false;
    int game_board_size = game_board->width*game_board->height;
    int empty_spots[game_board_size];
    int possible_spots = 0;


    for(int i = 0; i < game_board->height; i++) {
        for(int j = 0; j < game_board->width; j++) {
            if (game_board->board[i*game_board->width+j] == 0) {
                empty_spots[possible_spots] = (j + game_board->width*i);
                possible_spots++;
            }
        }
    }

    if(possible_spots != 0) {
      srand (time(NULL));
        ret = true;
        int random = rand();
        int random_spot = random % possible_spots;
        int x_pos = empty_spots[random_spot]%game_board->width;
        int y_pos = empty_spots[random_spot]/game_board->width;
        game_board->board[y_pos*game_board->width+x_pos] = 3;
    }

    return ret;
}

//renders a button to be displayed in the next frame, including the outline if currently selected
void drawButton(bool outline, int x, int y, char word[], int length, game_struct* game){
  if(outline){
    for(int i = 0; i < 60; i++){
            for(int j = 0; j < length*41; j++){
            if(j == 0 || j == 41*length-1 || i == 0 || i == 59){
                draw_pixel(j+x, i+y, 0x0, game->fb);
            }
          }
        }
  }
  x+=5;
  for(int i=0; i < length; i++){
    draw_char(x, y, word[i], 0x0, game->fb, game->fdes, game->scale);
    x+=40;
  }
}

//draws the score achieved in the game over screen, in case of versus mode, score of the winning snake is displayed
void drawScore(int score, game_struct* game){
  int x = 130;
  int y = 10;
  char print[11] = "SCORE:    ";
  char number[4];
  sprintf(number, "%d", score);
  memcpy(print+7, number, 3);
  drawButton(false, x, y, print, 10, game);
}

