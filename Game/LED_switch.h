#ifndef LED_SWITCH_H
#define LED_SWITCH_H

#include <stdbool.h>

void setLED(int r, int g, int b, bool left);

void LEDreset(void);

void diodsReset(void);

void diodsSet(int counter);

#endif