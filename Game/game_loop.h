#ifndef GAME_LOOP_H
#define GAME_LOOP_H

#include "utils.h"
#include <stdbool.h>

int gameLoop(game_struct* game, bool versus);

#endif