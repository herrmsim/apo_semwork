/*methods for menus triggered after gameplay, each method corresponds to one type of menu*/

#include <stdint.h>
#include "game_loop.h"
#include <stdint.h>
#include <stdlib.h>
#include "mzapo_regs.h"
#include "utils.h"
#include "mzapo_parlcd.h"
#include <time.h>
#include "font_types.h"
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include "LED_switch.h"

//return values: 0 =  resume game, 1 = go to main menu, 2 = game over, start new game

//triggered when a snake crashes, shows the achieved score --highest one in case of a versus mode
int game_over_screen(game_struct* game, int score) {
    struct timespec loop_delay;
    loop_delay.tv_sec = 0;
    loop_delay.tv_nsec = 150 * 1000 * 1000;
    char game_over[10] = "GAME OVER";
    char start_over[9] = "NEW GAME";
    char quit[6] = "LEAVE";
    bool outline;
    int choice = 0;
    int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
    int G_stat = ((r>>8)&0xff);

    while(1) {
        int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if ((r&0x7000000)!=0) {
            if(choice == 1){
                return 1;
            }else{
                return 2;
            }
        }
        if(G_stat-((r>>8)&0xff) >= 4 || G_stat-((r>>8)&0xff) <= -4){
            G_stat = ((r>>8)&0xff);
            choice = (choice == 1) ? 0 : 1;
        } 
        for (game->ptr = 0; game->ptr < 320*480 ; game->ptr++) {
            game->fb[game->ptr]=0xffff;
        }
        drawScore(score, game);
        drawButton(false, 90, 80, game_over, 9, game);
        outline = choice == 0 ? true : false;
        drawButton(outline, 60, 160, start_over, 8, game);
        drawButton(!outline, 150, 220, quit, 5, game);
        parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
        for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
        parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
        }

        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL); 
    }
    return 0;
}


//opens and handles pause menu options with 2 buttons: resume the game or return to the main menu
int pause_menu(game_struct* game, int score){
    struct timespec loop_delay;
    loop_delay.tv_sec = 0;
    loop_delay.tv_nsec = 150 * 1000 * 1000;
    char start[7] = "RESUME";
    char quit[6] = "LEAVE";
    int choice = 0;
    bool outline;
    int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
    int G_stat = ((r>>8)&0xff);
    while(1){
        int r = *(volatile uint32_t*)(game->mem_base + SPILED_REG_KNOBS_8BIT_o);
        if ((r&0x7000000)!=0) {
        if(choice == 0){
            sleep(1);
            return 0;
        }else{
            sleep(1);
            return 1;
        }
        }    
        if(G_stat-((r>>8)&0xff) >= 4 || G_stat-((r>>8)&0xff) <= -4){
        G_stat = ((r>>8)&0xff);
        choice = (choice == 0) ? 1 : 0;
        }
        for (game->ptr = 0; game->ptr < 320*480 ; game->ptr++) {
        game->fb[game->ptr]=0xffff;
        }
        outline = choice == 0 ? true : false;
        drawScore(score, game);
        drawButton(outline, 150, 100, start, 6, game);
        drawButton(!outline, 150, 170, quit, 5, game);
        parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
        for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
        parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
        }

        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL); 
    }
    return 0;
}

