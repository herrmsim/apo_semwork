/*metods to operate LED diod line below the LCD display and RGB LED 1 and 2*/
#include "mzapo_regs.h"
#include "LED_switch.h"
#include "mzapo_phys.h"

enum line {ZERO = 0, ONE = 1, TWO = 3, THREE = 7, FOUR = 15, FIVE = 31, SIX = 63, SEVEN = 127, EIGHT = 255};


//set the requested RGB LED to show light based on parameters r, g, b
void setLED(int r, int g, int b, bool left){
    unsigned char* led;
    unsigned char *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if(left){
        led = (mem_base + SPILED_REG_LED_RGB1_o);
    }else{
        led = (mem_base + SPILED_REG_LED_RGB2_o);
    }
    *led = (char)b;
    *(led+1) = (char)g;
    *(led+2) = (char)r;
    
}

//turn off both RGB LEDs
void LEDreset(void){
    unsigned char *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    unsigned char* led = mem_base + SPILED_REG_LED_RGB1_o;
    *led = (char)0;
    *(led+1) = (char)0;
    *(led+2) = (char)0;
    *(led+4) = (char)0;
    *(led+5) = (char)0;
    *(led+6) = (char)0;
}

//turn off all diods in the LED line
void diodsReset(void){
    unsigned char *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    unsigned char* diod = mem_base + SPILED_REG_LED_LINE_o;
    for(int i = 0; i < 4; i++){
        *(diod+i) = (char)ZERO;
    }
}

//turn on x diods starting from right, where x = counter
void diodsSet(int counter){
    counter-=3;
    if(counter > 32){
        return;
    }
    int full = counter/8;
    unsigned char *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    unsigned char* diod = mem_base + SPILED_REG_LED_LINE_o;
    int i = 0;
    for(i = 0; i < full; i++){
        *(diod+i) = (char)EIGHT;
    }
    int rest = counter%8;
    switch(rest){
        case 0:
            *(diod+i) = (char)ZERO;
            break;
        case 1:
            *(diod+i) = (char)ONE;
            break;
        case 2:
            *(diod+i) = (char)TWO;
            break;
        case 3:
            *(diod+i) = (char)THREE;
            break;
        case 4:
            *(diod+i) = (char)FOUR;
            break;
        case 5:
            *(diod+i) = (char)FIVE;
            break;
        case 6:
            *(diod+i) = (char)SIX;
            break;
        default:
            *(diod+i) = (char)SEVEN;
    }
}
