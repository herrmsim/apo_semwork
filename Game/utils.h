#ifndef UTILS_H
#define UTILS_H

#include "font_types.h"
#include <stdbool.h>

typedef struct{
  int ptr;
  unsigned char *mem_base;
  unsigned short *fb;
  unsigned char *parlcd_mem_base;
  int scale;
  font_descriptor_t *fdes;
  int speed;
} game_struct;

typedef struct{
    int* board;
    int width, height;
    bool fruit_present;
} game_space;

void draw_char(int x, int y, char ch, unsigned short color, unsigned short *fb, font_descriptor_t *fdes, int scale);

int char_width(int ch, unsigned short *fb, font_descriptor_t *fdes, int scale);

void draw_pixel_big(int x, int y, unsigned short color, unsigned short *fb, font_descriptor_t *fdes, int scale);

void draw_pixel(int x, int y, unsigned short color, unsigned short *fb);

bool spawn_fruit(game_space* game_board);

void drawButton(bool outline, int x, int y, char word[], int length, game_struct* game);

void drawScore(int score, game_struct* game);

#endif

