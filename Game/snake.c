/*methods regardig snake operation - initialization, growth and collision checks*/

#include "utils.h"
#include "snake.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define SIZE 20

//method to create a new player representation
snake_front* makeSnake(int playerNum){
    snake_front *player = malloc(sizeof(snake_front));
    if(!player){
        fprintf(stderr, "malloc error\n");
        return player;
    }
    player->died = false;
    player->ID = playerNum;
    player->direction = playerNum == 1 ? 0 : 2;
    player->tail = 0;
    player->head = 2;
    player->capacity = 5;
    player->size = 3;
    player->snake = calloc(player->capacity, sizeof(snake_part));
    switch(playerNum){
        case 1:
            for(int i = 0; i < player->size; i++){
                (player->snake+i)->x = 5-i;
                (player->snake+i)->y = 5;
            }
            break;
        case 2:
            for(int i = 0; i < player->size; i++){
                (player->snake+i)->x = 16+i;
                (player->snake+i)->y = 7;
            }
            break;
        default:
    }
    return player;
}

//checks if player is about to hit the LCD display border
bool checkCollisionBorder(snake_front*player){
    if((player->snake+player->head)->x < 0 || (player->snake+player->head)->y < 0 || 
        (player->snake+player->head)->x+1 > 480/SIZE || (player->snake+player->head)->y+1 > 320/SIZE){
        player->died = true;
        return true;
    }
    return false;
}

//checks if player has crashed into a body of a snake (its own body or the opponent in case of a versus mode)
bool checkCollisionPlayer(int x_pos, int y_pos, game_space* game_board){
    if(*(game_board->board+(y_pos*game_board->width+x_pos)) != 0){
        return true;
    }
    return false;
}

//increases size of the snake, reallocates memory if capacity has been reached
void growSnake(snake_front* player){
    player->size++;
    if(player->size >= player->capacity){
        for(int i = 0; i < player->size; i++){
        snake_part* new_snake = calloc(player->capacity*2, sizeof(snake_part));
        if(!new_snake){
            fprintf(stderr, "ERROR: calloc failed!\n");
        }
        int i = 0;
        int l = player->tail;
        do{
            *(new_snake+i) = *(player->snake+l);
            i++;
            l++;
            if(l >= player->capacity){
                l = 0;
            }
        }while(i < player->size);
        player->head = i-1;
        player->tail = 0;
        player->capacity*=2;
        free(player->snake);
        player->snake = new_snake;
        }
    }
}