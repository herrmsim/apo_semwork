#define _POSIX_C_SOURCE 200112L

 /*launches the game and opens the main menu, currently with two options: start the game or leave*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>            //termios, TCSANOW, ECHO, ICANON
 
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "utils.h"
#include "main_menus.h"
 
game_struct* buildGame(void);
void end(game_struct* game);
void printWelcome(game_struct* game);
 
 
int main(int argc, char *argv[]) {
  printf("Game launched\n");
  game_struct* game = buildGame();
  printWelcome(game);
  mainMenu(game);
  end(game);
  return 0;
}


//closes the program
void end(game_struct* game){

  parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
  for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
    parlcd_write_data(game->parlcd_mem_base, 0);
  }
 
  printf("Game closed\n");
 
  return;
}


//initialize game_struct
game_struct* buildGame(void){
  game_struct* game = malloc(sizeof(game_struct));
  if(!game){
    fprintf(stderr, "error, malloc failed!\n");
    return game;
  }
  game->scale = 4;
  game->fb  = (unsigned short *)malloc(320*480*2);
 
 
  game->parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (game->parlcd_mem_base == NULL)
    exit(1);
 
  game->mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (game->mem_base == NULL)
    exit(1);
 
  parlcd_hx8357_init(game->parlcd_mem_base);
 
  parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
  game->ptr=0;
  unsigned int c;
  int i,j;
  for (i = 0; i < 320 ; i++) {
    for (j = 0; j < 480 ; j++) {
      c = 0;
      game->fb[game->ptr]=c;
      parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr++]);
    }
  }
  return game;
}


//print welcome screen
void printWelcome(game_struct* game){
  game->fdes = &font_winFreeSystem14x16;
  for (game->ptr = 0; game->ptr < 320*480 ; game->ptr++) {
    game->fb[game->ptr]=0xffff;
  }
  int x = 100, y = 150;
  char welcome[8] = "Snakes";
  for(int i=0; i < 6; i++){
    draw_char(x, y, welcome[i], 0x0, game->fb, game->fdes, game->scale);
    x+=40;
  }
  parlcd_write_cmd(game->parlcd_mem_base, 0x2c);
  for (game->ptr = 0; game->ptr < 480*320 ; game->ptr++) {
    parlcd_write_data(game->parlcd_mem_base, game->fb[game->ptr]);
  }  
  sleep(3);
}


